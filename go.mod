module bitbucket.org/_metalogic_/lcat

go 1.15

require (
	bitbucket.org/_metalogic_/config v1.1.0
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/gorilla/websocket v1.4.1
)
