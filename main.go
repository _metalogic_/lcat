package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/log"
	"github.com/gorilla/websocket"
)

const (
	nanopersec = 1000000000
)

const (
	LCAT_TOKEN = "LCAT_TOKEN"
)

var (
	debugFlg     bool
	logsURL      string
	lcatToken    string
	timestampFlg int64
	windowFlg    string
	window       time.Duration
	fromFlg      string
	toFlg        string
	tzFlg        string
	now          time.Time
	location     *time.Location
	start        int64
	end          int64
	limit        int
	command      string
)

func init() {
	flag.StringVar(&logsURL, "url", "ws://localhost:8080", "URL of the logs server")

	flag.BoolVar(&debugFlg, "debug", false, "debug")
	flag.IntVar(&limit, "limit", 0, "limit size of query response")
	flag.Int64Var(&timestampFlg, "ts", 0, "print the record with a given timestamp")
	flag.StringVar(&windowFlg, "tw", "", "set the search window around the given timestamp (eg 2s, 300ms, 10ns ...)")
	flag.StringVar(&fromFlg, "from", "", "filter logs from datetime")
	flag.StringVar(&toFlg, "to", "", "filter logs to datetime")
	flag.StringVar(&tzFlg, "tz", "UTC", "the local timezone for from/to times and reporting")
}

func main() {
	flag.Parse()

	log.ClearFlag(log.LJSON)

	lcatToken = config.MustGetConfig(LCAT_TOKEN)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	u, err := url.Parse(logsURL)
	if err != nil {
		log.Fatalf("options: -url: invalid URL %s (%s)", logsURL, err)
	}
	if u.Scheme == "ws" {
		logsURL = logsURL + "/ws"
	} else if u.Scheme == "wss" {
		logsURL = logsURL + "/wss"
	} else {
		log.Fatal("options: -url: scheme must be ws or wss")
	}

	// debug logging for the lcat command
	if debugFlg {
		log.SetLevel(log.DebugLevel)
	}
	log.ClearFlag(log.LJSON)
	log.SetColorize(true)

	if tzFlg != "" {
		location, err = time.LoadLocation(tzFlg)
		if err != nil {
			if debugFlg {
				// dump .../local/go/lib/time/zoneinfo.zip
			}
			log.Fatalf("options: -tz '%s' is not a valid timezone", tzFlg)
		}
	}

	now = time.Now().In(location)
	if fromFlg != "" {
		start, err = getTimestamp(now, fromFlg, location)
		if err != nil {
			log.Fatalf("options: -from (%s)", err)
		}
		if toFlg != "" {
			end, err = getTimestamp(now, toFlg, location)
			if err != nil {
				log.Fatalf("options -to (%s)", err)
			}
			if end < start {
				log.Fatalf("options: start '%s' is after end '%s'", start, end)
			}
		}
		command = fmt.Sprintf("query from %d %d", start, end)
	}
	if fromFlg == "" && toFlg != "" {
		log.Fatal("options: -to cannot be provided without -from")
	}

	if timestampFlg > 0 && fromFlg != "" {
		log.Fatal("options: -ts cannot be combined with -from")
	}
	if windowFlg != "" {
		if timestampFlg == 0 {
			log.Fatalf("options: -tw a search window cannot be specified without a timestamp")
		}
		window, err = time.ParseDuration(windowFlg)
		if err != nil {
			log.Fatalf("options: -tw %s invalid duration (%s)", windowFlg, err)
		}
		command = fmt.Sprintf("query from %d %d", timestampFlg-window.Nanoseconds(), timestampFlg+window.Nanoseconds())
	}

	if windowFlg == "" && timestampFlg > 0 {
		command = fmt.Sprintf("query from %d %d", timestampFlg, timestampFlg)
	}

	log.Debugf("connecting to %s", logsURL)

	h := http.Header{"Authorization": {"Bearer " + lcatToken}}
	c, _, err := websocket.DefaultDialer.Dial(logsURL, h)
	if err != nil {
		log.Error("dial:", err)
		os.Exit(1)
	}
	defer c.Close()

	if limit > 0 && command == "" {
		log.Warning("limit is ignored when streaming")
	}

	if limit > 0 && command != "" {
		log.Debugf("executing command: 'limit %d'", limit)
		err = c.WriteMessage(websocket.TextMessage, []byte(fmt.Sprintf("limit %d", limit)))
		if err != nil {
			log.Fatalf("limit %d failed with %s", limit, err)
		}
	}

	// default is to stream - command exits on CTL-C
	if command == "" {
		command = "stream start"
	}

	log.Debugf("executing command: '%s'", command)
	if command == "stream start" {
		log.Info("hit CTL-C to exit")
	}
	err = c.WriteMessage(websocket.TextMessage, []byte(command))
	if err != nil {
		log.Fatalf("%s failed with %s", command, err)
	}

	done := make(chan struct{})

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Errorf("read: %s", err)
				return
			}
			if string(message) == io.EOF.Error() {
				fmt.Println()
				return
			}
			fmt.Println(string(message))
		}
	}()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {

		select {
		case <-done:
			return
		case t := <-ticker.C:
			err := c.WriteMessage(websocket.TextMessage, []byte(t.String()))
			if err != nil {
				log.Error("write:", err)
				return
			}
		case <-interrupt:
			log.Debug("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Error("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}
}

func getTimestamp(now time.Time, tstr string, location *time.Location) (timestamp int64, err error) {
	t, err := config.GetDatetimeInLocation(tstr, location)
	if err != nil {
		return timestamp, err
	}
	return t.UnixNano(), err
}
